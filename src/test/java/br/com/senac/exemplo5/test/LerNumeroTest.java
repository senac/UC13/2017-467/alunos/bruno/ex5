package br.com.senac.exemplo5.test;

import br.com.senac.exemplo5.LerNumero;
import org.junit.Test;
import static org.junit.Assert.*;

public class LerNumeroTest {

    public LerNumeroTest() {

    }

    @Test
    public void numero4DeveSerPar() {
        int valor = 4;
        boolean resultado = LerNumero.isPar(valor);
        assertTrue(resultado);

    }

    @Test
    public void numero5NaoDeveSerPar() {
        int valor = 5;
        boolean resultado = LerNumero.isPar(valor);
        assertFalse(resultado);
    }

    @Test
    public void numero10DeveSerPositivo() {
        int numero = 10;
        boolean resultado = LerNumero.isPositivo(numero);
        assertTrue(resultado);
    }

    @Test
    public void numero10NegDeveSerPositivo() {
        int numero = -10;
        boolean resultado = LerNumero.isPositivo(numero);
        assertFalse(resultado);

    }

}
