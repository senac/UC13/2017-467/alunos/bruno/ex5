package br.com.senac.exemplo5;

import java.util.Scanner;

public class LerNumero {

    public static void main(String[] args) {
        int valor;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite um valor: ");
        valor = scanner.nextInt();

        if (isPar(valor)) {
            System.out.println("O Valor é par.");
        } else {
            System.out.println("O Valor é impar.");
        }

        if (isPositivo(valor)) {
            System.out.println("É positivo.");
        } else {
            System.out.println("É negativo.");
        }
    }

    public static boolean isPar(int valor) {
        return valor % 2 == 0;
    }

    public static boolean isPositivo(int valor) {
        return valor >= 0;
    }

}
